import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandleAndSnapshot {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","C:\\TestLeaf\\eclipse-workspace\\Selenium\\driver\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		driver.findElementByPartialLinkText("Contact Us").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./Snaps/image1.jpeg");
		FileUtils.copyFile(src, obj);

		
	}

}
