package wdMethods;

import java.io.IOException;

//import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcel;

public class ProjectMethod extends SeMethods{
	
	public String dataSheetName;
	
	@BeforeSuite(groups= {"any"})
	public void beforeSuite() {
		startResult();
	}
	@Parameters({"url"})
	@BeforeMethod(groups= {"any"})
	public void login(String url/*, String username, String password*/) {
		testLevel();
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleLink = locateElement("linkText","CRM/SFA");
		click(eleLink);*/
	}
	
	@AfterMethod(groups= {"any"})
	public void closeTestBrowser() {
		closeAllBrowsers();
		
	}
	
	@AfterSuite(groups= {"any"})
	public void geneReport() {
		endResult();
	}
	
	@DataProvider(name = "fetchData")
	public Object[][] getData() throws IOException {
		
		Object[][] sheet = ReadExcel.Createlead(dataSheetName);
		return sheet;
		
		
	}
}

