package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.Alert;
//import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;
	
	public void generateReport() {
		startResult();
	}

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);	
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("pass", "The Browser Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser not launched");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linkText": return driver.findElementByLinkText(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}
		return null;
	}
	public  List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElements(ById.id(locValue));
			case "class": return driver.findElements(ByClassName.className(locValue));
			case "linkText": return driver.findElements(ByLinkText.linkText(locValue));
			case "xpath": return driver.findElements(ByXPath.xpath(locValue));
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Elements are Not Located ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {

		ele.sendKeys(data);
		System.out.println("The Data "+data+" is Entered Successfully");
		reportStep("pass", "Text entered Successfully");
		takeSnap();
		}
		catch (NoSuchElementException e) {
			reportStep("fail", "Text not entered successfully");
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();	
			reportStep("Pass", "Clicked successfully");
			takeSnap();
		}
		catch (NoSuchElementException e) {
			reportStep("Fail", "Unable to Click");
			takeSnap();
		}
		
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select dd= new Select(ele);
		dd.selectByIndex(index);
		System.out.println("The Dropdown is selected with "+index);

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String actualTitle= driver.getTitle();
		if (actualTitle.equals(expectedTitle)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		takeSnap();

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		takeSnap();
	}

	@Override
	public String getAlertText() {
		Alert alert = driver.switchTo().alert();
		return alert.getText();
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
