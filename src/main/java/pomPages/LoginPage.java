package pomPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;


public class LoginPage extends ProjectMethod  {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using= "username") WebElement eleUserName;
	@FindBy(how = How.ID,using= "password") WebElement elePassword;
	@FindBy(how= How.CLASS_NAME ,using= "decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterUserName(String uName) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleUserName,uName);
		return this;
		
	}
	public LoginPage enterPassword(String password) {
		type(elePassword,password);
		return this;
		
	}
	public HomePage clickLogin() {
		
		click(eleLogin);
		return new HomePage();

		
	}
	

}
