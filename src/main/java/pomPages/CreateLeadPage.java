package pomPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class CreateLeadPage extends ProjectMethod {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.ID, using = "createLeadForm_primaryEmail") WebElement eleEmailId;
	@FindBy(how = How.ID, using = "createLeadForm_primaryPhoneNumber") WebElement elePhoneNumber ;
	@FindBy(how= How.CLASS_NAME , using = "smallSubmit") WebElement eleClickSubmit;
	
	public CreateLeadPage enterCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String firstName) {
		type(eleFirstName, firstName );
		return this;
	}
	
	public CreateLeadPage enterLastName(String lastName) {
		type(eleLastName, lastName );
		return this;
	}
	public CreateLeadPage enterEmailId(String emailId) {
		type(eleEmailId, emailId );
		return this;
	}
	public CreateLeadPage enterContactNumber(String ContactNumber) {
		type(elePhoneNumber, ContactNumber );
		return this;
	}
	public ViewLeadPage clickSubmit() {
		click(eleClickSubmit);
		return new ViewLeadPage();
	}
	
	
	
}
