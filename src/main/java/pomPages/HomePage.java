package pomPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;


public class HomePage extends ProjectMethod {
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how= How.LINK_TEXT , using= "CRM/SFA") WebElement eleCrm;
	
	public MyHomePage clickCRMSFA() {
		click(eleCrm);
		return new MyHomePage();
		
	}

}
