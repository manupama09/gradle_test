package pomPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethod;

public class MyLeadsPage extends ProjectMethod {
	
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how= How.LINK_TEXT , using= "Create Lead") WebElement eleCreateLeads;
	
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLeads);
		return new CreateLeadPage();
		
	}

}